import unittest

import program
from program import tirer_au_sort


class TestPRogram(unittest.TestCase):

    def test_mon_programm(self):

        # Tirer au sort 50 phrases
        resultat = []
        for i in range(0, 50):
            resultat.append(tirer_au_sort())

        # Verifier que les phrases sont dans le tableau
        for r in resultat:
            self.assertTrue(r in program.phrases)

        # Verifier que les phrases sont aléatoires
        premier = resultat[0]
        del resultat[0]
        sum = 0

        for r in resultat:
            if premier != r:
                sum = sum + 1

        self.assertTrue(sum > 0)