FROM python:3-alpine

EXPOSE 5000

ADD requirements.txt /
RUN pip3 install -r requirements.txt

ADD program.py /

CMD ["python3", "program.py"]