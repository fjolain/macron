from random import randint

from flask import Flask

phrases = [
"Je traverse la rue, je vous trouve du travail",
"Des Gaulois réfractaires au changement",
"On met un pognon de dingue dans les minimas sociaux",
"Je ne céderai rien 'ni aux fainéants, ni aux cyniques'",
"Certains, au lieu de foutre le bordel, feraient mieux d'aller regarder s'ils ne peuvent pas avoir des postes ...",
"Les gens qui ne sont rien",
"La meilleure façon de se payer un costard, c'est de travailler",
"Les femmes salariées de Gad, 'pour beaucoup illettrés'",
"Le bus pourra 'bénéficier aux pauvres'",
"Le kwassa-kwassa pêche peu, il amène du Comorien"]

app = Flask(__name__)

@app.route("/")
def tirer_au_sort():
    depart = 0
    fin = len(phrases)-1

    i = randint(depart, fin)

    phrase = phrases[i]
    return phrase


if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0')